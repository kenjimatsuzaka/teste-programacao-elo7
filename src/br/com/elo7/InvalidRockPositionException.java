package br.com.elo7;

public class InvalidRockPositionException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private int x;
	private int y;

	public InvalidRockPositionException(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("Robot tried to access position (");
		sb.append(x + "," + y);
		sb.append(")");
		return sb.toString();
	}

}
