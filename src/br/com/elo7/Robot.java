package br.com.elo7;

import br.com.elo7.InvalidRockPositionException;
import br.com.elo7.CardinalDirection;
import br.com.elo7.Coordinate;
import br.com.elo7.Rock;

public class Robot {
	private Coordinate position;
	private CardinalDirection face;
	private Rock rock;

	public Robot(int x, int y, CardinalDirection face, Rock rock) {
		if(isValidPosition(x, y, rock)) {
			this.rock = rock;
			this.position = new Coordinate(x, y);
			this.face = face;
		} else {
			throw new IllegalArgumentException("Invalid starting position: (" + x + "," + y + ")");
		}
	}
	
	public void move(String instructions) {
		for (int i = 0; i < instructions.length(); i++) {
			this.move(instructions.charAt(i));
		}
	}
	
	private void move(char instruction) {
		switch (instruction) {
		case 'L':
			int ordinalLeft = (this.face.ordinal() - 1 + 4) % 4;
			this.face = CardinalDirection.values()[ordinalLeft];
			break;
		case 'R':
			int ordinalRight = (this.face.ordinal() + 1) % 4;
			this.face = CardinalDirection.values()[ordinalRight];
			break;
		case 'M':
			executeMoveAction();
			break;
		}
	}
	
	private void executeMoveAction() {
		switch (this.face) {
		case N:
			this.moveToNorth();
			break;
		case E:
			this.moveToEast();
			break;
		case S:
			this.moveToSouth();
			break;
		case W:
			this.moveToWest();
			break;
		}
	}
	
	private void moveToEast() {
		int newX = this.position.getX() + 1;
		if (newX <= this.rock.getMaxPos().getX()) {
			this.position.setX(newX);
		} else {
			throw new InvalidRockPositionException(newX, this.position.getY());
		}
	}

	private void moveToWest() {
		int newX = this.position.getX() - 1;
		if (newX >= 0) {
			this.position.setX(newX);
		} else {
			throw new InvalidRockPositionException(newX, this.position.getY());
		}
	}
	
	private void moveToNorth() {
		int newY = this.position.getY() + 1;
		if (newY <= this.rock.getMaxPos().getY()) {
			this.position.setY(newY);
		} else {
			throw new InvalidRockPositionException(this.position.getX(), newY);
		}
	}

	private void moveToSouth() {
		int newY = this.position.getY() - 1;
		if (newY >= 0) {
			this.position.setY(newY);
		} else {
			throw new InvalidRockPositionException(this.position.getX(), newY);
		}
	}

	private boolean isValidPosition(int x, int y, Rock rock) {
		return x >= 0 && x <= rock.getMaxPos().getX() && y >= 0 && y <= rock.getMaxPos().getY();
	}

	public Coordinate getPosition() {
		return position;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}

	public CardinalDirection getFace() {
		return face;
	}

	public void setFace(CardinalDirection face) {
		this.face = face;
	}

	public Rock getRock() {
		return rock;
	}

	public void setRock(Rock rock) {
		this.rock = rock;
	}

}
