package br.com.elo7;

import static org.junit.Assert.*;

import org.junit.Test;

public class RobotTest {

	@Test(expected=IllegalArgumentException.class)
	public void creatingNewRobotWithInvalidXPosition() {
		Rock rock = new Rock(5, 5);
		int x = -1;
		int y = 0;
		CardinalDirection face = CardinalDirection.valueOf("E");
		Robot robot = new Robot(x, y, face, rock);
	}

	@Test(expected=IllegalArgumentException.class)
	public void creatingNewRobotWithInvalidYPosition() {
		Rock rock = new Rock(5, 5);
		int x = 0;
		int y = 6;
		CardinalDirection face = CardinalDirection.valueOf("S");
		Robot robot = new Robot(x, y, face, rock);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void creatingNewRobotWithInvalidCardinalDirection() {
		Rock rock = new Rock(5, 5);
		int x = 0;
		int y = 0;
		CardinalDirection face = CardinalDirection.valueOf("T");
		Robot robot = new Robot(x, y, face, rock);
	}
	
	@Test
	public void creatingNewRobot() {
		Rock rock = new Rock(5, 5);
		int x = 0;
		int y = 0;
		CardinalDirection face = CardinalDirection.valueOf("N");
		Robot robot = new Robot(x, y, face, rock);
		assert robot != null;
	}
	
	@Test
	public void testCase1() {
		Rock rock = new Rock(5, 5);
		int x = 1;
		int y = 2;
		CardinalDirection face = CardinalDirection.valueOf("N");
		String instruction = "LMLMLMLMM";
		Robot result = executeTestCase(x, y, face, rock, instruction);
		
		assert result != null;
		assert result.getPosition().getX() == 1;
		assert result.getPosition().getY() == 3;
		assert result.getFace().toString() == "N";
	}
	
	@Test
	public void testCase2() {
		Rock rock = new Rock(5, 5);
		int x = 3;
		int y = 3;
		CardinalDirection face = CardinalDirection.valueOf("E");
		String instruction = "MMRMMRMRRM";
		Robot result = executeTestCase(x, y, face, rock, instruction);
		
		assert result != null;
		assert result.getPosition().getX() == 5;
		assert result.getPosition().getY() == 1;
		assert result.getFace().toString() == "E";
	}
	
	@Test(expected=InvalidRockPositionException.class)
	public void testCase3() {
		Rock rock = new Rock(5, 5);
		int x = 5;
		int y = 5;
		CardinalDirection face = CardinalDirection.valueOf("E");
		String instruction = "M";
		Robot result = executeTestCase(x, y, face, rock, instruction);
	}
	
	@Test(expected=InvalidRockPositionException.class)
	public void testCase4() {
		Rock rock = new Rock(5, 5);
		int x = 0;
		int y = 0;
		CardinalDirection face = CardinalDirection.valueOf("E");
		String instruction = "RM";
		Robot result = executeTestCase(x, y, face, rock, instruction);
	}
	
	@Test(expected=InvalidRockPositionException.class)
	public void testCase5() {
		Rock rock = new Rock(5, 5);
		int x = 5;
		int y = 5;
		CardinalDirection face = CardinalDirection.valueOf("E");
		String instruction = "LLMMMMMM";
		Robot result = executeTestCase(x, y, face, rock, instruction);
	}
	
	@Test
	public void testCase6() {
		Rock rock = new Rock(5, 5);
		int x = 5;
		int y = 5;
		CardinalDirection face = CardinalDirection.valueOf("E");
		String instruction = "LLLLLL";
		Robot result = executeTestCase(x, y, face, rock, instruction);
		
		assert result != null;
		assert result.getPosition().getX() == 5;
		assert result.getPosition().getY() == 5;
		assert result.getFace().toString() == "N";
	}
	
	@Test
	public void testCase7() {
		Rock rock = new Rock(5, 5);
		int x = 5;
		int y = 5;
		CardinalDirection face = CardinalDirection.valueOf("E");
		String instruction = "";
		Robot result = executeTestCase(x, y, face, rock, instruction);
		
		assert result != null;
		assert result.getPosition().getX() == 5;
		assert result.getPosition().getY() == 5;
		assert result.getFace().toString() == "E";
	}
	
	private Robot executeTestCase(int x, int y, CardinalDirection face, Rock rock, String instruction) {
		Robot robot = new Robot(x, y, face, rock);
		robot.move(instruction);
		return robot;
	}
}
