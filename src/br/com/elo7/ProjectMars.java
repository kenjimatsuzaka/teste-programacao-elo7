package br.com.elo7;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.com.elo7.CardinalDirection;
import br.com.elo7.Robot;
import br.com.elo7.Rock;

public class ProjectMars {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Rock rock = null;
		
		if(sc.hasNextLine()) {
			String[] line = sc.nextLine().split("\\s+");
			int x = Integer.parseInt(line[0]);
			int y = Integer.parseInt(line[1]);
			rock = new Rock(x, y);
		}
		
		List<Robot> listOfRobots = new ArrayList<Robot>();
		while(sc.hasNextLine()) {
			String[] line = sc.nextLine().split("\\s+");
			int startingX = Integer.parseInt(line[0]);
			int startingY = Integer.parseInt(line[1]);
			String face = line[2];
			String instructions = sc.nextLine();
			
			Robot robot = new Robot(startingX, startingY, CardinalDirection.valueOf(face), rock);
			robot.move(instructions);
			
			listOfRobots.add(robot);
		}
		sc.close();
		
		for(Robot robot : listOfRobots) {
			System.out.printf("%d %d %s\n", robot.getPosition().getX(), robot.getPosition().getY(), robot.getFace());
		}
	}

}
