package br.com.elo7;

import br.com.elo7.Coordinate;

public class Rock {
	private Coordinate maxPos;
	
	public Rock(int x, int y) {
		this.maxPos = new Coordinate(x, y);
	}

	public Coordinate getMaxPos() {
		return maxPos;
	}

	public void setMaxPos(Coordinate maxPos) {
		this.maxPos = maxPos;
	}

}
